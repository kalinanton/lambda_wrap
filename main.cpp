//
//  main.cpp
//  gcjv
//
//  Created by Антон Калинин on 12/07/2019.
//  Copyright © 2019 Антон Калинин. All rights reserved.
//

#include <iostream>
#include <memory>

template<class... Args> struct typelist;
template<class Head, class... Tail> struct typelist<Head, Tail...> {
    using head = Head;
    using tail = typelist<Tail...>;
};
template<class A> struct typelist<typelist<A>> {
    using head = typename typelist<A>::head;
    using tail = typename typelist<A>::tail;
};
template<class A> struct typelist<A> {
    using head = A;
    using tail = typelist<void>;
};
template<class typelist> struct size {
    static constexpr int value = 1 + size<typename typelist::tail>::value;
};
template<> struct size<typelist<void>> {
    static constexpr int value = 0;
};

template<class typelist, int i> struct get {
    using value = typename get<typename typelist::tail, i - 1>::value;
};
template<class typelist> struct get<typelist, 0> {
    using value = typename typelist::head;
};

template<class lambda_functor>
struct lambda_describer {
    template<typename ...> struct list;
    template<typename ret_type, typename C, typename... arg_type> struct list<ret_type(C::*)(arg_type...)> {
        using ret = ret_type;
        using arg = typelist<arg_type...>;
    };
    template<typename ret_type, typename C, typename... arg_type> struct list<ret_type(C::*)(arg_type...) const> {
        using ret = ret_type;
        using arg = typelist<arg_type...>;
    };
    using current_list = list<decltype(&lambda_functor::operator())>;
    using ret = typename current_list::ret;
    using arg = typename current_list::arg;
};

template<class ret_type, class arg_type_list>
struct lambda_wrap_I {
public:
    using ret = ret_type;
    using arg = arg_type_list;
protected:
    template<bool is_enough, int... numbers>
    struct operator_holder {
        virtual ret operator() (typename get<arg, numbers>::value && ...) = 0;
        virtual ~operator_holder() = default;
        using type = operator_holder<is_enough, numbers...>;
    };
    
    template<int... numbers>
    struct operator_holder<false, numbers...> :
    public operator_holder<sizeof...(numbers) + 1 == size<arg>::value, numbers..., sizeof...(numbers)> {
        using type = operator_holder<sizeof...(numbers) + 1 == size<arg>::value, numbers..., sizeof...(numbers)>;
    };
    typename operator_holder<size<arg>::value == 0>::type * holder;
    
public:
    lambda_wrap_I(typename operator_holder<false>::type * holder_C_pointer) : holder(holder_C_pointer) {};
    virtual ~lambda_wrap_I() { delete holder; }
    template<class... args_T> auto operator() (args_T && ... args) {
        return (*holder)(std::forward<args_T>(args)...);
    }
};

template<class lambda_functor> struct lambda_wrap_C : public lambda_wrap_I<
    typename lambda_describer<lambda_functor>::ret,
    typename lambda_describer<lambda_functor>::arg> {
private:
        using ret = typename lambda_describer<lambda_functor>::ret;
        using arg = typename lambda_describer<lambda_functor>::arg;
        using parent = lambda_wrap_I<ret, arg>;
public:
        lambda_wrap_C(lambda_functor && _lambda) : parent(new typename operator_holder<size<arg>::value == 0>::type (std::forward<lambda_functor>(_lambda)))
        {};
        
        lambda_wrap_C(const lambda_wrap_C<lambda_functor> & _src) = delete;
        lambda_wrap_C() = delete;
private:
        template<bool is_enough, int... numbers>
        struct operator_holder : public parent::template operator_holder<is_enough, numbers...> {
            lambda_functor lambda;
            operator_holder(lambda_functor && _lambda) : lambda(_lambda) {};
            virtual ret operator() (typename get<arg, numbers>::value && ... args) override {
                return lambda(std::forward<typename get<arg, numbers>::value>(args) ...);
            };
            
            using type = operator_holder<true, numbers...>;
        };
        
        template<int... numbers>
        struct operator_holder<false, numbers...> : public parent::template operator_holder<false, numbers...> {
            using type = typename operator_holder<sizeof...(numbers) + 1 == size<arg>::value, numbers..., sizeof...(numbers)>::type;
        };
};

template<class wrapper_C> struct function_wrapper_pointer : protected std::shared_ptr<wrapper_C> {
public:
    template<class... args> typename wrapper_C::ret operator() (args && ... a) {
        return this->operator*().operator()(std::forward<args>(a)...);
    }
    function_wrapper_pointer(wrapper_C * pointer) : std::shared_ptr<wrapper_C>(pointer) {};
};

template<class functor_T>
function_wrapper_pointer<lambda_wrap_I<typename lambda_describer<functor_T>::ret, typename lambda_describer<functor_T>::arg>> make_functor_wrapping(functor_T && func) {
    return function_wrapper_pointer<lambda_wrap_I<typename lambda_describer<functor_T>::ret, typename lambda_describer<functor_T>::arg>>(new lambda_wrap_C<functor_T>(std::forward<functor_T>(func)));
}

int main(int argc, const char * argv[]) {
    char p = 's';
    
    int a_real = 1;
    
    auto funct_wrapping1 = make_functor_wrapping([&p](int & a) {
        p+=a;
        return p;
    });
    
    std::cout << funct_wrapping1(a_real);
    
    funct_wrapping1 = make_functor_wrapping([](int & a) {
        a-=2;
        return 'f';
    });
    
    std::cout << funct_wrapping1(a_real);
    
    return 0;
}
